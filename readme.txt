=== Strip-shortcodes-from-posts ===
Contributors: ManxStef
Donate link: https://femsplain.com/
Tags: shortcodes, posts
Requires at least: 3.0.1
Tested up to: 4.4.1
Stable tag: 0.1
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin, when activated, IMMEDIATELY AND PERMANENTLY strips all shortcodes from all Posts. USE WITH CAUTION!

== Description ==

This plugin, when activated, IMMEDIATELY AND PERMANENTLY strips all shortcodes from all Posts. USE WITH CAUTION!

== Installation ==

1. Upload the strip-shortcodes-from-posts folder to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

BACK UP YOUR DATA before activating this plugin. There are no take-backs!

== Screenshots ==

1. This screen shot description corresponds to screenshot-1.(png|jpg|jpeg|gif). Note that the screenshot is taken from
the /assets directory or the directory that contains the stable readme.txt (tags or trunk). Screenshots in the /assets
directory take precedence. For example, `/assets/screenshot-1.png` would win over `/tags/4.3/screenshot-1.png`
(or jpg, jpeg, gif).
2. This is the second screen shot

== Changelog ==

= 0.1 =
This is the initial release.