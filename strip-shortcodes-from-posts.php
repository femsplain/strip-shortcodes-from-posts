<?php
/**
 * Plugin Name: Strip-shortcodes-from-posts
 * Version: 0.1-alpha
 * Description: IMMEDIATELY AND PERMANENTLY strips all shortcodes from all Posts. USE WITH CAUTION!
 * Author: Stef Pause
 * Author URI: https://femsplain.com
 * Plugin URI: 
 * Text Domain: strip-shortcodes-from-posts
 * @package Strip-shortcodes-from-posts
 */

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

function fmspln_remove_all_shortcodes_from_db() {
	global $wpdb;
	$posts = $wpdb->get_results( "SELECT ID, post_title, post_content FROM $wpdb->posts WHERE post_type = 'post'" );
	foreach ( $posts as $post ) {
		$contentstripped = strip_shortcodes( $post->post_content );
		$wpdb->update( $wpdb->posts, array( 'post_content' => $contentstripped ), array( 'ID' => $post->ID ) );
	}
}
register_activation_hook( __FILE__, 'fmspln_remove_all_shortcodes_from_db' );
